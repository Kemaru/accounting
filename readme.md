# Account ING
### Building and running

```
mvn compile
mvn package
java -jar {jarName}.jar
```

### Configuration

The .properties file needs to be in the same directory as the .jar when starting the app.

| property | description |
| ------ | ------ |
|spring.data.mongodb.host | host for the mongo db|
| spring.data.mongodb.port | port for mongo db|
| spring.data.mongodb.database | db name |
| accounting.client.host | wiremock host |
| accounting.client.port | wiremock port [defaults to 8080] |
| accounting.username.default | used if no username is specified for the /sync API [defaults to Ionescu] |
| accounting.sync.interval.minutes | interval between synchronizations expressedin minutes [defaults to 1440] |

### Endpoints

 * GET /account : 
    * returns all accounts
 * GET /account/{id} : 
    * returns the account with the given id
    * throws a validation exception if id=INVALID
 * GET /transaction : 
    * return all transactions
 * GET / transaction/{id} : 
    * return the trasaction with the give id
    * throws a validation exception if id=INVALID
 * POST /sync?userName=Dan&syncInterval=5
    * triggers synchronization
    * userName parameter is optional
    * syncInterval is optional
    * if syncInterval is present then the automated synchronization will be scheduled at the new interval

 

