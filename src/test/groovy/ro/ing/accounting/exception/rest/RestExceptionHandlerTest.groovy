package ro.ing.accounting.exception.rest

import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerResponse
import ro.ing.accounting.exception.SyncPendingException
import ro.ing.accounting.exception.ValidationException
import ro.ing.accounting.exception.config.ExceptionDefinitionHolder
import ro.ing.accounting.exception.config.ExceptionDetails
import spock.lang.Specification

class RestExceptionHandlerTest extends Specification {

    RestExceptionHandler restExceptionHandler;
    ExceptionDefinitionHolder definitionHolder;

    def static final SYNC_EX_DEF = ExceptionDetails.builder()
            .errorCode("SYNC_ERR")
            .retryable(true)
            .clazz(SyncPendingException.class)
            .status(HttpStatus.CONFLICT)
            .build()

    def static final VALID_EX_DET = ExceptionDetails.builder()
            .errorCode("VALID_ERR")
            .retryable(false)
            .clazz(ValidationException.class)
            .status(HttpStatus.BAD_REQUEST)
            .build()

    static final Map<Class<Throwable>,ExceptionDetails> DETAILS = new HashMap<>();

    void setup() {

        definitionHolder = Spy(ExceptionDefinitionHolder);

        definitionHolder.registerExceptionDetails(SYNC_EX_DEF)
        DETAILS.put(SYNC_EX_DEF.clazz,SYNC_EX_DEF)
        definitionHolder.registerExceptionDetails(VALID_EX_DET)
        DETAILS.put(VALID_EX_DET.clazz,VALID_EX_DET)

        restExceptionHandler = new RestExceptionHandler(definitionHolder)
    }

    def "Handle"() {

        given:"an exception"
            def exception = excClass.getConstructor(String).newInstance(excMessage)

        when:"the exception is received bey the exception handler"
        ServerResponse response = restExceptionHandler.handle(exception).block()

        then:"the exception details are extracted from the definition holder"
            1* definitionHolder.getDetailsFor(excClass)

        and:"the response contains the expected result"
            response.entity.code      == DETAILS.get(excClass).getErrorCode()
            response.entity.message   == excMessage
            response.entity.retryable == DETAILS.get(excClass).isRetryable()
        where:
            excClass             | excMessage
            ValidationException  | "Test validation message"
            SyncPendingException | "Test sync message"
    }
}
