package ro.ing.accounting.handler

import org.quartz.Scheduler
import org.quartz.Trigger
import org.quartz.TriggerBuilder
import org.springframework.web.reactive.function.server.ServerRequest
import ro.ing.accounting.exception.SyncPendingException
import ro.ing.accounting.synchronization.SynchronizationManager
import spock.lang.Specification

import static org.quartz.SimpleScheduleBuilder.simpleSchedule

class SynchronizationHandlerTest extends Specification {

    SynchronizationHandler synchronizationHandler
    SynchronizationManager manager
    Trigger trigger
    Scheduler scheduler

    static final String USER_PARAM_NAME     = "userName"
    static final String USER_NAME           = "GIGEL"
    static final String INTERVAL_PARAM_NAME = "syncInterval"

    void setup() {

        manager = Mock(SynchronizationManager)
        scheduler = Mock(Scheduler)
        trigger =  TriggerBuilder.newTrigger()
                .withDescription("trigger description")
                .withIdentity("test trigger identity")
                .withSchedule(simpleSchedule().repeatForever().withIntervalInMinutes(1))
                .build();
        synchronizationHandler = new SynchronizationHandler(manager,trigger,scheduler)
    }

    def "startSync"() {
        given:
            def mockRequest = Mock(ServerRequest)
        when:
            synchronizationHandler.startSync(mockRequest)
        then:
            1* manager.isSyncPending() >> false
            1* manager.startSync(USER_NAME)
            1* mockRequest.queryParam(USER_PARAM_NAME) >> Optional.of(USER_NAME)
            1* mockRequest.queryParam(INTERVAL_PARAM_NAME) >> Optional.empty()
    }

    def "startSync - pending"() {
        given:
            def mockRequest = Mock(ServerRequest)
        when:
            synchronizationHandler.startSync(mockRequest).block()
        then:
            1* manager.isSyncPending() >> true
            0 * manager.startSync(any())
            1* mockRequest.queryParam(USER_PARAM_NAME) >> Optional.of(USER_PARAM_NAME)
            1* mockRequest.queryParam(INTERVAL_PARAM_NAME) >> Optional.empty()
            thrown SyncPendingException
    }

    def "startSync - modify schedule"() {
        given:
            def mockRequest = Mock(ServerRequest)
        when:
            synchronizationHandler.startSync(mockRequest).block()
        then:
            1* manager.isSyncPending() >> false
            1* manager.startSync(USER_NAME)
            1* mockRequest.queryParam(USER_PARAM_NAME) >> Optional.of(USER_NAME)
            1* mockRequest.queryParam(INTERVAL_PARAM_NAME) >> Optional.of("3")
            1* scheduler.rescheduleJob(trigger.getKey(),TriggerBuilder.newTrigger()
                    .withDescription(trigger.getDescription())
                    .withIdentity(trigger.getKey())
                    .withSchedule(simpleSchedule().repeatForever().withIntervalInMinutes(3))
                    .build())
    }


}
