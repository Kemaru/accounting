package ro.ing.accounting.scheduling;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.springframework.stereotype.Component;
import ro.ing.accounting.exception.SyncPendingException;
import ro.ing.accounting.logging.Log;
import ro.ing.accounting.synchronization.SynchronizationManager;

@AllArgsConstructor
@Component
@Slf4j
public class SyncJob implements Job {

    private SynchronizationManager synchronizationManager;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        log.info("Sync job started. Next fire time at: " +jobExecutionContext.getNextFireTime());
        if(!synchronizationManager.isSyncPending()) {
            synchronizationManager.startSync("");

        }else{
            throw new SyncPendingException("Synchronization pending.");
        }
    }

}
