package ro.ing.accounting.scheduling;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

@Configuration
public class ScheduleConfig {

    @Bean
    public JobDetail jobDetail() {
        return JobBuilder.newJob().ofType(SyncJob.class)
                .storeDurably()
                .withIdentity("Sync_Job_Detail")
                .withDescription("Invoke Sync Job service...")
                .build();
    }
    @Bean
    public Trigger trigger(JobDetail job, @Value("${accounting.sync.interval.minutes:1440}") Integer interval) {
        return TriggerBuilder.newTrigger().forJob(job)
                .withIdentity("sync_trigger")
                .withDescription("Sync trigger")
                .withSchedule(simpleSchedule().repeatForever().withIntervalInMinutes(interval))
                .build();
    }
}
