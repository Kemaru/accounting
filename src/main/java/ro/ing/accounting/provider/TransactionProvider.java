package ro.ing.accounting.provider;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ro.ing.accounting.data.db.repository.entity.TransactionDocument;
import ro.ing.accounting.model.Transaction;

public interface TransactionProvider {

    Flux<Transaction> getTransactions();
    Mono<Transaction> getTransaction(String id);
    Flux<TransactionDocument> saveTransactions(Flux<Transaction> transactions);
    Mono<Void> clearTransactions();
}
