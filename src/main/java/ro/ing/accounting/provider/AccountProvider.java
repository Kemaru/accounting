package ro.ing.accounting.provider;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ro.ing.accounting.data.db.repository.entity.AccountDocument;
import ro.ing.accounting.model.Account;

public interface AccountProvider {

    Mono<Account> getAccount(String id);
    Flux<Account> getAccounts();
    Flux<AccountDocument> saveAccounts(Flux<Account> accounts);
    Mono<Void> clearAccounts();
}
