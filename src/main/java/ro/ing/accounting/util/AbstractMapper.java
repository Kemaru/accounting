package ro.ing.accounting.util;

import java.util.function.Function;

public abstract class AbstractMapper <T,R> implements Function<T,R> {

    protected abstract R mapInternal(T original);

    public R apply(T source){
        return source == null ? null :
                mapInternal(source);
    }
}
