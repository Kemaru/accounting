package ro.ing.accounting.router;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import ro.ing.accounting.handler.AccountHandler;
import ro.ing.accounting.handler.SynchronizationHandler;
import ro.ing.accounting.handler.TransactionHandler;
import ro.ing.accounting.exception.rest.RestExceptionHandler;

import java.util.Objects;

import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.method;

@Configuration
@EnableWebFlux
@AllArgsConstructor
public class RouterConfig {

    private RestExceptionHandler restExceptionHandler;

    @Bean
    public RouterFunction<ServerResponse> router(AccountHandler accountHandler, SynchronizationHandler synchronizationHandler, TransactionHandler transactionHandler){
        return RouterFunctions.route()
                .onError(Throwable.class, (throwable, serverRequest) -> restExceptionHandler.handle(throwable))
                .path("/account",builder->builder
                        .nest(accept(MediaType.APPLICATION_JSON), builder2 -> builder2
                                .GET("/{id}", accountHandler::getAccount)
                                .GET("/",accountHandler::getAccounts)))
                .path("/transaction", builder -> builder
                        .nest(accept(MediaType.APPLICATION_JSON).and(method(HttpMethod.GET)), b2->b2
                                .GET("/{id}", transactionHandler::getTransaction)
                                .GET("/",transactionHandler::getTransactions)))
                .path("/sync",builder -> builder.POST("/",synchronizationHandler::startSync))
                .build();

    }
}
