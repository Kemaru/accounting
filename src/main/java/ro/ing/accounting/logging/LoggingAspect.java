package ro.ing.accounting.logging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Component
@Aspect
public class LoggingAspect {

    private static final ObjectMapper OM;
    private static final String BEFORE_PREFIX_TEMPLATE = "Calling method %s.%s%s";
    private static final String AFTER_PREFIX_TEMPLATE = "Method %s.%s ended%s";
    static {
        OM = new ObjectMapper();
        OM.registerModule(new JavaTimeModule());
    }

    @Around("@annotation(ro.ing.accounting.logging.Log)")
    public Object logMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getStaticPart().getSignature();

        Logger logger = LoggerFactory.getLogger(signature.getDeclaringTypeName());

        String[] parameterNames = signature.getParameterNames();
        Object[] args = joinPoint.getArgs();

        List<String> parameterStrings = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            String value = "****";
            try {
                value = OM.writerWithDefaultPrettyPrinter().writeValueAsString(args[i]);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            parameterStrings.add(parameterNames[i] + "=" +value);
        }

        String parameterValuesStr = CollectionUtils.isEmpty(parameterStrings) ? "()" : " with parameters:" + System.lineSeparator() + String.join(",", parameterStrings);

        logger.info(String.format(BEFORE_PREFIX_TEMPLATE,signature.getDeclaringType().getSimpleName(),signature.getName(),parameterValuesStr));

        Object methodResult = joinPoint.proceed();

        return logResult(methodResult,signature,logger);
    }

    private Object logResult(Object methodResult,MethodSignature signature,Logger logger) throws Throwable{
        if(methodResult instanceof Mono) {
            return ((Mono<?>) methodResult).doOnNext(o -> logPublisher(logger,signature,o));
        }else if(methodResult instanceof Flux){
            return ((Flux<?>) methodResult).doOnNext(o -> logPublisher(logger,signature,o));
        }else{
            String resultString = signature.getReturnType().getName().equals("void") ? "" : " with result:" +System.lineSeparator() + OM.writerWithDefaultPrettyPrinter().writeValueAsString(methodResult);
            logger.info(String.format(AFTER_PREFIX_TEMPLATE,signature.getDeclaringType().getSimpleName(),signature.getName(),resultString));
            return methodResult;
        }
    }

    private <R> R logPublisher(Logger logger, MethodSignature signature, R result){
        try {
            String resultString = " with result:" + System.lineSeparator() + OM.writerWithDefaultPrettyPrinter().writeValueAsString(result);
            logger.info(String.format(AFTER_PREFIX_TEMPLATE, signature.getDeclaringType().getSimpleName(), signature.getName(), resultString));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}
