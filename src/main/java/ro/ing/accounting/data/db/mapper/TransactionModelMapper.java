package ro.ing.accounting.data.db.mapper;

import ro.ing.accounting.data.db.repository.entity.TransactionDocument;
import ro.ing.accounting.model.Amount;
import ro.ing.accounting.model.CreditSubject;
import ro.ing.accounting.model.ExchangeRate;
import ro.ing.accounting.model.Transaction;
import ro.ing.accounting.util.AbstractMapper;

public class TransactionModelMapper extends AbstractMapper<TransactionDocument,Transaction> {

    public static final TransactionModelMapper INSTANCE = new TransactionModelMapper();

    private TransactionModelMapper(){}

    @Override
    protected Transaction mapInternal(TransactionDocument doc){
        return Transaction.builder()
                .accountId(doc.getAccountId())
                .id(doc.getTxId())
                .amount(doc.getAmount())
                .creditor(CreditSubject.builder().maskedPan(doc.getCreditorMaskedPan()).name(doc.getCreditorName()).build())
                .currency(doc.getCurrency())
                .debtor(CreditSubject.builder().maskedPan(doc.getDebtorMaskedPan()).name(doc.getDebtorName()).build())
                .description(doc.getDescription())
                .exchangeRate(ExchangeRate.builder().currencyFrom(doc.getCurrencyFrom()).currencyTo(doc.getCurrencyTo()).rate(doc.getRate()).build())
                .originalAmount(Amount.builder().amount(doc.getOriginalAmount()).currency(doc.getOriginalCurrency()).build())
                .status(doc.getStatus())
                .update(doc.getUpdate())
                .build();
    }
}
