package ro.ing.accounting.data.db;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ro.ing.accounting.data.db.mapper.AccountDocumentMapper;
import ro.ing.accounting.data.db.mapper.AccountModelMapper;
import ro.ing.accounting.data.db.repository.AccountRepository;
import ro.ing.accounting.data.db.repository.entity.AccountDocument;
import ro.ing.accounting.logging.Log;
import ro.ing.accounting.model.Account;
import ro.ing.accounting.provider.AccountProvider;

@Service
@AllArgsConstructor
public class AccountProviderImpl implements AccountProvider {

    private AccountRepository accountRepository;

    @Override
    @Log
    public Mono<Account> getAccount(String id) {
        return accountRepository.findByAccId(id).map(AccountModelMapper.INSTANCE);
    }

    @Override
    public Flux<Account> getAccounts() {
        return accountRepository.findAll().map(AccountModelMapper.INSTANCE);
    }

    @Override
    @Log
    public Flux<AccountDocument> saveAccounts(Flux<Account> accounts) {

        return accounts.map(AccountDocumentMapper.INSTANCE).collectList().flatMapMany(accountRepository::saveAll);
    }

    @Override
    public Mono<Void> clearAccounts() {
        return accountRepository.deleteAll();
    }
}
