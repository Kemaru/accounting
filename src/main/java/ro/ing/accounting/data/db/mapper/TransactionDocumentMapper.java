package ro.ing.accounting.data.db.mapper;

import ro.ing.accounting.data.db.repository.entity.TransactionDocument;
import ro.ing.accounting.model.Transaction;
import ro.ing.accounting.util.AbstractMapper;

public class TransactionDocumentMapper extends AbstractMapper<Transaction, TransactionDocument> {

    public static final TransactionDocumentMapper INSTANCE = new TransactionDocumentMapper();

    private TransactionDocumentMapper(){}

    @Override
    protected TransactionDocument mapInternal(Transaction model){
        return TransactionDocument.builder()
                .accountId(model.getAccountId())
                .txId(model.getId())
                .amount(model.getAmount())
                .creditorMaskedPan(model.getCreditor().getMaskedPan())
                .creditorName(model.getCreditor().getName())
                .currency(model.getCurrency())
                .debtorMaskedPan(model.getDebtor().getMaskedPan()).debtorName(model.getDebtor().getName())
                .description(model.getDescription())
                .currencyFrom(model.getExchangeRate().getCurrencyFrom())
                .currencyTo(model.getExchangeRate().getCurrencyTo())
                .rate(model.getExchangeRate().getRate())
                .originalAmount(model.getOriginalAmount().getAmount())
                .originalCurrency(model.getOriginalAmount().getCurrency())
                .status(model.getStatus())
                .update(model.getUpdate())
                .build();
    }
}
