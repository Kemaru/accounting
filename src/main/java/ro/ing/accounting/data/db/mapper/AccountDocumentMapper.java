package ro.ing.accounting.data.db.mapper;

import ro.ing.accounting.data.db.repository.entity.AccountDocument;
import ro.ing.accounting.model.Account;
import ro.ing.accounting.util.AbstractMapper;

public class AccountDocumentMapper extends AbstractMapper<Account, AccountDocument> {

    public static final AccountDocumentMapper INSTANCE = new AccountDocumentMapper();

    private AccountDocumentMapper(){}

    @Override
    protected AccountDocument mapInternal(Account model){
        return AccountDocument.builder()
                .accId(model.getId())
                .balance(model.getBalance())
                .name(model.getName())
                .product(model.getProduct())
                .status(model.getStatus())
                .type(model.getType())
                .dateUpdated(model.getUpdate())
                .build();
    }
}
