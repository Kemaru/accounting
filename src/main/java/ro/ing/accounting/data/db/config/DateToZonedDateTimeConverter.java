package ro.ing.accounting.data.db.config;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

public class DateToZonedDateTimeConverter implements Converter<String, ZonedDateTime> {

        public ZonedDateTime convert(String value) {

            return ZonedDateTime.from(DateTimeFormatter.ISO_DATE_TIME.parseBest(value, ZonedDateTime::from, LocalDateTime::from));
        }
    }