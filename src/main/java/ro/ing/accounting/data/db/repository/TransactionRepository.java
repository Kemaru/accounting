package ro.ing.accounting.data.db.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ro.ing.accounting.data.db.repository.entity.TransactionDocument;

@Repository
public interface TransactionRepository extends ReactiveCrudRepository<TransactionDocument,Integer> {

    Mono<TransactionDocument> findByTxId(String txId);
}
