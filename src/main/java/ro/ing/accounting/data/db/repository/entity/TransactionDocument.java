package ro.ing.accounting.data.db.repository.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class TransactionDocument implements Serializable {

    @Id
    private String id;

    private String txId;
    private String accountId;

    private String     currencyFrom;
    private String     currencyTo;
    private BigDecimal rate;

    private BigDecimal originalAmount;
    private String     originalCurrency;

    private BigDecimal amount;
    private String     currency;

    private String creditorMaskedPan;
    private String creditorName;

    private String debtorMaskedPan;
    private String debtorName;

    private String        status;
    private ZonedDateTime update;
    private String        description;
}
