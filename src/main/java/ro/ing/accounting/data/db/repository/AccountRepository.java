package ro.ing.accounting.data.db.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import ro.ing.accounting.data.db.repository.entity.AccountDocument;

@Repository
public interface AccountRepository extends ReactiveCrudRepository<AccountDocument,Integer> {

    Mono<AccountDocument> findByAccId(String accId);
}
