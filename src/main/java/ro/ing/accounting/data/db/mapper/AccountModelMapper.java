package ro.ing.accounting.data.db.mapper;

import ro.ing.accounting.data.db.repository.entity.AccountDocument;
import ro.ing.accounting.model.Account;
import ro.ing.accounting.util.AbstractMapper;

public class AccountModelMapper extends AbstractMapper<AccountDocument, Account> {

    public static final AccountModelMapper INSTANCE = new AccountModelMapper();

    private AccountModelMapper(){}

    @Override
    protected Account mapInternal(AccountDocument model){
        return Account.builder()
                .id(model.getAccId())
                .balance(model.getBalance())
                .name(model.getName())
                .product(model.getProduct())
                .status(model.getStatus())
                .type(model.getType())
                .update(model.getDateUpdated())
                .build();
    }
}
