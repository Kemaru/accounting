package ro.ing.accounting.data.db;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ro.ing.accounting.data.db.mapper.TransactionDocumentMapper;
import ro.ing.accounting.data.db.mapper.TransactionModelMapper;
import ro.ing.accounting.data.db.repository.TransactionRepository;
import ro.ing.accounting.data.db.repository.entity.TransactionDocument;
import ro.ing.accounting.logging.Log;
import ro.ing.accounting.model.Transaction;
import ro.ing.accounting.provider.TransactionProvider;

@Service
@AllArgsConstructor
public class TransactionProviderImpl implements TransactionProvider{

    private TransactionRepository transactionRepository;

    @Override
    @Log
    public Flux<Transaction> getTransactions() {
        return transactionRepository.findAll().map(TransactionModelMapper.INSTANCE);
    }

    @Override
    @Log
    public Mono<Transaction> getTransaction(String id) {
        return transactionRepository.findByTxId(id).map(TransactionModelMapper.INSTANCE);
    }

    @Override
    @Log
    public Flux<TransactionDocument> saveTransactions(Flux<Transaction> transactions) {
        return transactionRepository.saveAll(transactions.map(TransactionDocumentMapper.INSTANCE));
    }

    @Override
    public Mono<Void> clearTransactions() {
        return transactionRepository.deleteAll();
    }
}
