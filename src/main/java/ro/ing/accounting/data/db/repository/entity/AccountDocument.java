package ro.ing.accounting.data.db.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document("accounts")
public class AccountDocument implements Serializable {

    @Id
    private String id;

    private String accId;
    private String name;
    private String product;
    private String status;
    private String type;

    private ZonedDateTime dateUpdated;
    private BigDecimal balance;
}
