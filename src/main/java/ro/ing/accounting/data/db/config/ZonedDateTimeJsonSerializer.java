package ro.ing.accounting.data.db.config;

import org.springframework.core.convert.converter.Converter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

class ZonedDateTimeJsonSerializer implements Converter<ZonedDateTime, String> {
        @Override
        public String convert(ZonedDateTime value) {
            return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value);
        }
    }