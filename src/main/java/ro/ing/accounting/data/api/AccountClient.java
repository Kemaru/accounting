package ro.ing.accounting.data.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ro.ing.accounting.data.api.dto.AccountClientDto;
import ro.ing.accounting.data.api.dto.TokenClientDto;
import ro.ing.accounting.data.api.dto.TransactionClientDto;
import ro.ing.accounting.data.api.mapper.AccountClientMapper;
import ro.ing.accounting.data.api.mapper.TransactionClientMapper;
import ro.ing.accounting.model.Account;
import ro.ing.accounting.model.Transaction;

import java.net.URI;

@Component
public class AccountClient {

    private WebClient webClient;
    private static final String ACCOUNT_PATH     = "/accounts";
    private static final String TRANSACTION_PATH = "/transactions";
    private static final String LOGIN_PATH       = "/login";

    private final String url;
    private final String port;

    private static final String TOKEN_HEADER_NAME = "X-AUTH";
    private static final String USER_HEADER_NAME  = "username";

    public AccountClient(@Value("${accounting.client.host}")String url,
                         @Value("${accounting.client.port:8080}") String port,
                         WebClient webClient) {
        this.webClient = webClient;

        this.url  = url;
        this.port = port;
    }

    public Flux<Account> getAccounts(String token){

        var uri = UriComponentsBuilder.fromUriString(url).port(port).path(ACCOUNT_PATH).build().toUri();
        return webClient.get()
                .uri(uri)
                .header(TOKEN_HEADER_NAME,token)
                .retrieve()
                .bodyToFlux(AccountClientDto.class)
                .map(AccountClientMapper.INSTANCE);
    }

    public Flux<Transaction> getTransactions(String token){

        var uri = UriComponentsBuilder.fromUriString(url).port(port).path(TRANSACTION_PATH).build().toUri();
        return webClient.get()
                .uri(uri)
                .header(TOKEN_HEADER_NAME,token)
                .retrieve()
                .bodyToFlux(TransactionClientDto.class)
                .map(TransactionClientMapper.INSTANCE);
    }

    public Mono<String> getToken(String userName){
        var uri = UriComponentsBuilder.fromUriString(url).port(port).path(LOGIN_PATH).build().toUri();

        return webClient.post()
                .uri(uri)
                .header(USER_HEADER_NAME,userName)
                .retrieve()
                .bodyToMono(TokenClientDto.class)
                .map(TokenClientDto::getToken);
    }
}
