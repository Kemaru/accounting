package ro.ing.accounting.data.api.mapper;

import ro.ing.accounting.data.api.dto.CreditSubjectClientDto;
import ro.ing.accounting.model.CreditSubject;
import ro.ing.accounting.util.AbstractMapper;

public class CreditSubjectClientMapper extends AbstractMapper<CreditSubjectClientDto,CreditSubject> {

    public static final CreditSubjectClientMapper INSTANCE = new CreditSubjectClientMapper();

    private CreditSubjectClientMapper(){}

    @Override
    protected CreditSubject mapInternal(CreditSubjectClientDto dto){
        return CreditSubject.builder()
                        .maskedPan(dto.getMaskedPan())
                        .name(dto.getName())
                        .build();
    }
}
