package ro.ing.accounting.data.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import ro.ing.accounting.data.api.RequestLoggingFilterFunction;
import ro.ing.accounting.data.api.WebClientFactory;

@Configuration
public class ClientConfig {

    private Integer timeout;

    public ClientConfig(@Value("${accounting.client.timeout.seconds:30}") Integer timeout) {
        this.timeout = timeout;
    }

    @Bean
    public WebClient webClient(){

        return WebClientFactory.getFactory()
                .setTimeout(timeout)
                .addRequestFilter(new RequestLoggingFilterFunction())
                .buildClient();
    }
}