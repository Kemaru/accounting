package ro.ing.accounting.data.api.mapper;

import ro.ing.accounting.data.api.dto.AmountClientDto;
import ro.ing.accounting.model.Amount;
import ro.ing.accounting.util.AbstractMapper;

public class AmountClientMapper extends AbstractMapper<AmountClientDto,Amount>{

    public static final AmountClientMapper INSTANCE = new AmountClientMapper();

    private AmountClientMapper(){}

    @Override
    protected  Amount mapInternal(AmountClientDto source) {
        return Amount.builder()
                .amount(source.getAmount())
                .currency(source.getCurrency())
                .build();
    }
}
