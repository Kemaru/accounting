package ro.ing.accounting.data.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.ClientRequest;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@Slf4j
public class RequestLoggingFilterFunction implements Function<ClientRequest,Mono<ClientRequest>> {

    @Override
    public Mono<ClientRequest> apply(ClientRequest clientRequest) {
        log.info("Client making a {} request to path: {}",clientRequest.method(),clientRequest.url().getPath());
        return Mono.just(clientRequest);
    }
}