package ro.ing.accounting.data.api.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExchangeRateClientDto implements Serializable{
    private String currencyFrom;
    private String currencyTo;

    private BigDecimal rate;
}
