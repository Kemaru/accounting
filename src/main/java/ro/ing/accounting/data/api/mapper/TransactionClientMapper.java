package ro.ing.accounting.data.api.mapper;

import ro.ing.accounting.data.api.dto.TransactionClientDto;
import ro.ing.accounting.model.Transaction;
import ro.ing.accounting.util.AbstractMapper;

public class TransactionClientMapper extends AbstractMapper<TransactionClientDto,Transaction> {

    public static final TransactionClientMapper INSTANCE = new TransactionClientMapper();

    private TransactionClientMapper(){}

    @Override
    protected Transaction mapInternal(TransactionClientDto dto){
        return Transaction.builder()
                .accountId(dto.getAccountId())
                .id(dto.getId())
                .amount(dto.getAmount())
                .creditor(CreditSubjectClientMapper.INSTANCE.apply(dto.getCreditor()))
                .currency(dto.getCurrency())
                .debtor(CreditSubjectClientMapper.INSTANCE.apply(dto.getDebtor()))
                .description(dto.getDescription())
                .exchangeRate(ExchangeRateClientMapper.INSTANCE.apply(dto.getExchangeRate()))
                .originalAmount(AmountClientMapper.INSTANCE.apply(dto.getOriginalAmount()))
                .status(dto.getStatus())
                .update(dto.getUpdate())
                .build();
    }
}
