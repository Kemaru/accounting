package ro.ing.accounting.data.api;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WebClientFactory {

    private Set<Function<ClientRequest, Mono<ClientRequest>>> requestFilters = new HashSet<>();
    private Set<Function<ClientResponse, Mono<ClientResponse>>> responseFilters = new HashSet<>();
    private Integer timeout = 30;
    private TimeUnit timeUnit = TimeUnit.SECONDS;


    private WebClientFactory() {
    }

    public static WebClientFactory getFactory() {
        return new WebClientFactory();
    }

    public WebClient buildClient() {
        var tcpClient = TcpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 30000)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .doOnConnected(c -> c.addHandlerLast(new ReadTimeoutHandler(timeout, timeUnit))
                        .addHandlerLast(new WriteTimeoutHandler(timeout, timeUnit))
                );

        ClientHttpConnector httpConnector = new ReactorClientHttpConnector(HttpClient.from(tcpClient));

        var builder = WebClient.builder().clientConnector(httpConnector);
        requestFilters.forEach(filter -> builder.filter(ExchangeFilterFunction.ofRequestProcessor(filter)));
        responseFilters.forEach(filter -> builder.filter(ExchangeFilterFunction.ofResponseProcessor(filter)));

        return builder.build();
    }

    public WebClientFactory addRequestFilter(Function<ClientRequest, Mono<ClientRequest>> requestFilter) {
        requestFilters.add(requestFilter);
        return this;
    }

    public WebClientFactory addResponseFilter(Function<ClientResponse, Mono<ClientResponse>> responseFilter) {
        responseFilters.add(responseFilter);
        return this;
    }

    public WebClientFactory setTimeout(Integer timeout) {
        this.timeout = timeout;
        return this;
    }

    public WebClientFactory setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        return this;
    }
}