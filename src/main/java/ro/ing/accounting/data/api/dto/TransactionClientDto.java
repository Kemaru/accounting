package ro.ing.accounting.data.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionClientDto implements Serializable{

    private String id;
    private String accountId;

    private ExchangeRateClientDto exchangeRate;
    private AmountClientDto       originalAmount;
    private BigDecimal            amount;
    private String                currency;

    private CreditSubjectClientDto creditor;
    private CreditSubjectClientDto debtor;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime update;
    private String        status;
    private String        description;
}
