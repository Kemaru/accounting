package ro.ing.accounting.data.api.mapper;

import ro.ing.accounting.data.api.dto.AccountClientDto;
import ro.ing.accounting.model.Account;
import ro.ing.accounting.util.AbstractMapper;

public class AccountClientMapper extends AbstractMapper<AccountClientDto,Account> {

    public static final AccountClientMapper INSTANCE = new AccountClientMapper();

    private AccountClientMapper(){}

    @Override
    protected Account mapInternal(AccountClientDto dto){
        return Account.builder()
                .id(dto.getId())
                .balance(dto.getBalance())
                .name(dto.getName())
                .product(dto.getProduct())
                .status(dto.getStatus())
                .type(dto.getType())
                .update(dto.getUpdate())
                .build();
    }
}
