package ro.ing.accounting.data.api.dto;

import lombok.*;

import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreditSubjectClientDto implements Serializable{

    private String maskedPan;
    private String name;
}
