package ro.ing.accounting.data.api.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AmountClientDto implements Serializable{

    private BigDecimal amount;
    private String     currency;
}
