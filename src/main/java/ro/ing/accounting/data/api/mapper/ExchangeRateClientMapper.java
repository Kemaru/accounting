package ro.ing.accounting.data.api.mapper;

import ro.ing.accounting.data.api.dto.ExchangeRateClientDto;
import ro.ing.accounting.model.ExchangeRate;
import ro.ing.accounting.util.AbstractMapper;

public class ExchangeRateClientMapper extends AbstractMapper<ExchangeRateClientDto,ExchangeRate> {

    public static final ExchangeRateClientMapper INSTANCE = new ExchangeRateClientMapper();

    private ExchangeRateClientMapper(){}

    @Override
    protected ExchangeRate mapInternal(ExchangeRateClientDto dto){
        return ExchangeRate.builder()
                .currencyFrom(dto.getCurrencyFrom())
                .currencyTo(dto.getCurrencyTo())
                .build();
    }
}
