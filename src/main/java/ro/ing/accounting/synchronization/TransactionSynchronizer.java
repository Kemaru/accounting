package ro.ing.accounting.synchronization;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import ro.ing.accounting.data.api.AccountClient;
import ro.ing.accounting.logging.Log;
import ro.ing.accounting.model.Transaction;
import ro.ing.accounting.provider.TransactionProvider;



@Component
public class TransactionSynchronizer implements Synchronizer{

    private String defaultUserName;

    private AccountClient       accountClient;
    private TransactionProvider transactionProvider;

    private boolean pendingSync;

    public TransactionSynchronizer(@Value("${accounting.username.default:Ionescu}")String defaultUserName, AccountClient accountClient, TransactionProvider transactionProvider) {
        this.defaultUserName = defaultUserName;
        this.accountClient = accountClient;
        this.transactionProvider = transactionProvider;
    }

    @Override
    public boolean isSyncPending() {
        return pendingSync;
    }

    @Override
    @Log
    public void sync(String userName) {
        pendingSync = true;

        var user = StringUtils.isEmpty(userName) ? defaultUserName : userName;

        transactionProvider.clearTransactions().subscribe();

        Flux<Transaction> transactionFlux = accountClient.getToken(user)
                .flatMapMany(accountClient::getTransactions);

        transactionProvider.saveTransactions(transactionFlux).last().subscribe(o -> pendingSync = false);
    }
}
