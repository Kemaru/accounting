package ro.ing.accounting.synchronization;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ro.ing.accounting.logging.Log;

import java.util.Set;

@Component
@AllArgsConstructor
public class SynchronizationManager {

    private Set<Synchronizer> synchronizers;

    @Log
    public void startSync(String userName){
        synchronizers.forEach(synchronizer -> synchronizer.sync(userName));
    }

    @Log
    public boolean isSyncPending(){
        return synchronizers.stream().anyMatch(Synchronizer::isSyncPending);
    }
}
