package ro.ing.accounting.synchronization;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import ro.ing.accounting.data.api.AccountClient;
import ro.ing.accounting.logging.Log;
import ro.ing.accounting.model.Account;
import ro.ing.accounting.provider.AccountProvider;

@Component
public class AccountSynchronizer implements Synchronizer{

    private String defaultUserName;

    private AccountClient   accountClient;
    private AccountProvider accountProvider;

    private boolean pendingSync;

    public AccountSynchronizer(@Value("${accounting.username.default:Ionescu}")String defaultUserName, AccountClient accountClient, AccountProvider accountProvider) {
        this.defaultUserName = defaultUserName;
        this.accountClient   = accountClient;
        this.accountProvider = accountProvider;
    }

    @Override
    public boolean isSyncPending() {
        return pendingSync;
    }

    @Override
    @Log
    public void sync(String userName){

        pendingSync = true;

        var user = StringUtils.isEmpty(userName) ? defaultUserName : userName;

        accountProvider.clearAccounts().subscribe();

        Flux<Account> newAccounts = accountClient.getToken(user)
                .flatMapMany(accountClient::getAccounts);

        accountProvider.saveAccounts(newAccounts).last().subscribe(m -> pendingSync = false);
    }

}
