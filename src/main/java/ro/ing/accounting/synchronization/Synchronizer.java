package ro.ing.accounting.synchronization;

public interface Synchronizer {

    boolean isSyncPending();
    void sync(String userName);
}
