package ro.ing.accounting.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ro.ing.accounting.exception.ValidationException;
import ro.ing.accounting.logging.Log;
import ro.ing.accounting.model.Account;
import ro.ing.accounting.provider.AccountProvider;

@Service
@AllArgsConstructor
public class AccountService {

    private AccountProvider accountProvider;

    private static String INVALID_ID = "INVALID";
    @Log
    public Flux<Account> getAccounts(){
        return accountProvider.getAccounts();
    }

    @Log
    public Mono<Account> getAccount(String id) {

        return INVALID_ID.equals(id) ?
                Mono.error(new ValidationException("Invalid id.")):
                accountProvider.getAccount(id);
    }
}
