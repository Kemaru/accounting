package ro.ing.accounting.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ro.ing.accounting.exception.ValidationException;
import ro.ing.accounting.logging.Log;
import ro.ing.accounting.model.Transaction;
import ro.ing.accounting.provider.TransactionProvider;

@AllArgsConstructor
@Service
public class TransactionService {

    private TransactionProvider transactionProvider;

    private static String INVALID_ID = "INVALID";

    @Log
    public Flux<Transaction> getTransactions(){
        return transactionProvider.getTransactions();
    }

    @Log
    public Mono<Transaction> getTransaction(String id){
        return INVALID_ID.equals(id) ?
                Mono.error(new ValidationException("Invalid id.")):
                transactionProvider.getTransaction(id);
    }
}
