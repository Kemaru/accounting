package ro.ing.accounting.model;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class AccountQueryRequest implements Serializable{

    private String name;
    private String product;
    private String status;
    private String type;
}
