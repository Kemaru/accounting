package ro.ing.accounting.model;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExchangeRate implements Serializable{
    private String currencyFrom;
    private String currencyTo;

    private BigDecimal rate;
}
