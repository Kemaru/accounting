package ro.ing.accounting.model;

import lombok.*;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Account {

    private String id;
    private String name;
    private String product;
    private String status;
    private String type;

    private ZonedDateTime update;
    private BigDecimal    balance;
}
