package ro.ing.accounting.model;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Amount implements Serializable{

    private BigDecimal amount;
    private String     currency;
}
