package ro.ing.accounting.model;

import lombok.*;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Transaction {

    private String id;
    private String accountId;

    private ExchangeRate exchangeRate;
    private Amount       originalAmount;
    private BigDecimal   amount;
    private String       currency;

    private CreditSubject creditor;
    private CreditSubject debtor;

    private String        status;
    private ZonedDateTime update;
    private String        description;
}
