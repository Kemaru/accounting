package ro.ing.accounting.model;

import lombok.*;

import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreditSubject implements Serializable{

    private String maskedPan;
    private String name;
}
