package ro.ing.accounting.handler;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ro.ing.accounting.handler.mapper.TransactionDtoMapper;
import ro.ing.accounting.service.TransactionService;

@Component
@AllArgsConstructor
public class TransactionHandler {

    private TransactionService transactionService;

    public Mono<ServerResponse> getTransaction(ServerRequest request){
        return transactionService.getTransaction(request.pathVariable("id"))
                .map(TransactionDtoMapper.INSTANCE)
                .flatMap(account -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(account))
                        .switchIfEmpty(ServerResponse.notFound().build()));
    }

    public Mono<ServerResponse> getTransactions(ServerRequest request){

        return transactionService.getTransactions()
                .collectList()
                .flatMap(transactions -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(transactions)));

    }
}
