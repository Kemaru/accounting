package ro.ing.accounting.handler.dto;

import lombok.*;
import ro.ing.accounting.model.Amount;
import ro.ing.accounting.model.CreditSubject;
import ro.ing.accounting.model.ExchangeRate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class TransactionDto implements Serializable {

    private String id;
    private String accountId;

    private ExchangeRate exchangeRate;
    private Amount       originalAmount;
    private BigDecimal   amount;
    private String       currency;

    private CreditSubject creditor;
    private CreditSubject debtor;

    private String        status;
    private ZonedDateTime update;
    private String        description;
}
