package ro.ing.accounting.handler.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class AccountDto implements Serializable{

    private String id;
    private String name;
    private String product;
    private String status;
    private String type;

    private ZonedDateTime update;
    private BigDecimal balance;
}
