package ro.ing.accounting.handler.mapper;

import ro.ing.accounting.model.Account;
import ro.ing.accounting.handler.dto.AccountDto;
import ro.ing.accounting.util.AbstractMapper;

public class AccountDtoMapper extends AbstractMapper<Account,AccountDto> {

    public static final AccountDtoMapper INSTANCE = new AccountDtoMapper();

    private AccountDtoMapper(){}

    @Override
    protected AccountDto mapInternal(Account source){
        return AccountDto.builder()
                .id(source.getId())
                .balance(source.getBalance())
                .name(source.getName())
                .product(source.getProduct())
                .status(source.getStatus())
                .type(source.getType())
                .update(source.getUpdate())
                .build();
    }
}
