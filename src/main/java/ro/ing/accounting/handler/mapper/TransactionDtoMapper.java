package ro.ing.accounting.handler.mapper;

import ro.ing.accounting.model.Transaction;
import ro.ing.accounting.handler.dto.TransactionDto;
import ro.ing.accounting.util.AbstractMapper;

public class TransactionDtoMapper extends AbstractMapper<Transaction, TransactionDto> {

    public static final TransactionDtoMapper INSTANCE = new TransactionDtoMapper();

    private TransactionDtoMapper(){}

    @Override
    protected TransactionDto mapInternal(Transaction dto){
        return TransactionDto.builder()
                .accountId(dto.getAccountId())
                .id(dto.getId())
                .amount(dto.getAmount())
                .creditor(dto.getCreditor())
                .currency(dto.getCurrency())
                .debtor(dto.getDebtor())
                .description(dto.getDescription())
                .exchangeRate(dto.getExchangeRate())
                .originalAmount(dto.getOriginalAmount())
                .status(dto.getStatus())
                .update(dto.getUpdate())
                .build();
    }
}
