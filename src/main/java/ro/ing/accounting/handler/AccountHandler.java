package ro.ing.accounting.handler;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ro.ing.accounting.handler.mapper.AccountDtoMapper;
import ro.ing.accounting.service.AccountService;

@Component
@AllArgsConstructor
public class AccountHandler {

    private AccountService accountService;

    public Mono<ServerResponse> getAccount(ServerRequest request){
        return accountService.getAccount(request.pathVariable("id"))
                .map(AccountDtoMapper.INSTANCE)
                .flatMap(account -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(account))
                        .switchIfEmpty(ServerResponse.notFound().build()));
    }

    public Mono<ServerResponse> getAccounts(ServerRequest request){

        return accountService.getAccounts()
                .collectList()
                .flatMap(accounts -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(accounts)));

    }
}
