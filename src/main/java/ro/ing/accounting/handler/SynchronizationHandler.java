package ro.ing.accounting.handler;

import lombok.AllArgsConstructor;
import org.quartz.*;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ro.ing.accounting.exception.SyncPendingException;
import ro.ing.accounting.exception.ValidationException;
import ro.ing.accounting.synchronization.SynchronizationManager;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

@Component
@AllArgsConstructor
public class SynchronizationHandler {

    private SynchronizationManager synchronizationManager;
    private Trigger trigger;
    private Scheduler scheduler;

    public Mono<ServerResponse> startSync (ServerRequest request){

        var userName = request.queryParam("userName").orElse(null);
        request.queryParam("syncInterval").ifPresent(syncInterval -> {

            int interval;
            try{

                interval = Integer.parseInt(syncInterval);
            }catch (NumberFormatException e){
                throw new ValidationException("Invalid syncInterval value.");
            }

            Trigger newTrigger = TriggerBuilder.newTrigger()
                    .withDescription(trigger.getDescription())
                    .withIdentity(trigger.getKey())
                    .withSchedule(simpleSchedule().repeatForever().withIntervalInMinutes(interval))
                    .build();


            try {
                scheduler.rescheduleJob(TriggerKey.triggerKey(trigger.getKey().getName(),trigger.getKey().getGroup()),newTrigger);
            } catch (SchedulerException e) {
                throw new ValidationException("Job rescheduling failed", e);
            }
        });

        if(synchronizationManager.isSyncPending()){
            return Mono.error(new SyncPendingException("Synchronization pending."));
        }else{
            synchronizationManager.startSync(userName);
            return ServerResponse.accepted().build();
        }
    }
}
