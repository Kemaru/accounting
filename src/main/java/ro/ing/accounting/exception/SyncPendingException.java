package ro.ing.accounting.exception;

import org.springframework.http.HttpStatus;

@ExceptionDefinition(retryable = true,errorCode = "SYNC_PENDING",status = HttpStatus.BAD_REQUEST)
public class SyncPendingException extends RuntimeException{

    public SyncPendingException() {
    }

    public SyncPendingException(String message) {
        super(message);
    }

    public SyncPendingException(String message, Throwable cause) {
        super(message, cause);
    }
}
