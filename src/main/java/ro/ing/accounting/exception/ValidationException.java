package ro.ing.accounting.exception;

import org.springframework.http.HttpStatus;

@ExceptionDefinition(errorCode = "VALIDATION_ERR",status = HttpStatus.BAD_REQUEST)
public class ValidationException extends RuntimeException{

    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
