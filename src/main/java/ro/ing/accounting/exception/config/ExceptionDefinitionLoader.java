package ro.ing.accounting.exception.config;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import ro.ing.accounting.exception.ExceptionDefinition;

@Configuration
public class ExceptionDefinitionLoader implements BeanPostProcessor{

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if(bean instanceof ExceptionDefinitionHolder) {
            try (ScanResult scanResult = new ClassGraph().enableAnnotationInfo().acceptPackages("ro.ing.accounting.exception").scan()) {
                for (ClassInfo classInfo : scanResult.getClassesWithAnnotation(ExceptionDefinition.class.getName())) {
                    ExceptionDefinition annotation = (ExceptionDefinition) classInfo.getAnnotationInfo(ExceptionDefinition.class.getName()).loadClassAndInstantiate();

                    String errCode = annotation.errorCode().isEmpty() ? classInfo.getSimpleName() : annotation.errorCode();
                    boolean retryable = annotation.retryable();
                    HttpStatus status = annotation.status();
                    Class clazz = classInfo.loadClass();

                    ((ExceptionDefinitionHolder) bean).registerExceptionDetails(ExceptionDetails.builder()
                            .errorCode(errCode)
                            .retryable(retryable)
                            .clazz(clazz)
                            .status(status)
                            .build());
                }
            }
        }
        return bean;
    }
}
