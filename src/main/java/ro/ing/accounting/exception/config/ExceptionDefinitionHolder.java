package ro.ing.accounting.exception.config;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class ExceptionDefinitionHolder {

    private Map<Class<Throwable>,ExceptionDetails> exceptionDetailMap = new HashMap<>();

    public void registerExceptionDetails(ExceptionDetails details){
        exceptionDetailMap.put(details.getClazz(),details);
    }

    public Optional<ExceptionDetails> getDetailsFor(Class clazz){
        return Optional.ofNullable(exceptionDetailMap.get(clazz));
    }
}
