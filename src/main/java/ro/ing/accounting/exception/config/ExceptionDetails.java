package ro.ing.accounting.exception.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ExceptionDetails {

    private String errorCode;
    private boolean retryable;
    private HttpStatus status;
    private Class<Throwable> clazz;
}
