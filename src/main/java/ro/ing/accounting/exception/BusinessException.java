package ro.ing.accounting.exception;

import org.springframework.http.HttpStatus;

@ExceptionDefinition(errorCode = "BSN_ERR",status = HttpStatus.BAD_REQUEST)
public class BusinessException extends RuntimeException{
    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
