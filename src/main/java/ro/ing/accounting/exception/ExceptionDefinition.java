package ro.ing.accounting.exception;


import org.springframework.http.HttpStatus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
public @interface ExceptionDefinition {

    String errorCode() default "";
    boolean retryable() default false;
    HttpStatus status() default HttpStatus.INTERNAL_SERVER_ERROR;

}
