package ro.ing.accounting.exception.rest;


import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExceptionModel implements Serializable{

	private static final long serialVersionUID = -1938360979334757972L;

	private String code;
	private String message;
	private boolean retryable;
}
