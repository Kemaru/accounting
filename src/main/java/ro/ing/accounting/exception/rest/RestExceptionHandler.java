package ro.ing.accounting.exception.rest;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import ro.ing.accounting.exception.config.ExceptionDefinitionHolder;

@Slf4j
@AllArgsConstructor
@Component
public class RestExceptionHandler {

	private final ExceptionDefinitionHolder exceptionDefinitionHolder;

	public Mono<ServerResponse> handle(Throwable ex){
		logError(ex);

		return exceptionDefinitionHolder.getDetailsFor(ex.getClass())
				.map(det -> ServerResponse.status(det.getStatus())
						.bodyValue(ExceptionModel.builder()
								.code(det.getErrorCode())
								.retryable(det.isRetryable())
								.message(ex.getMessage())
								.build()))
				.orElse(ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.bodyValue(ExceptionModel.builder()
								.code("ERR")
								.retryable(false)
								.message(ex.getMessage())
								.build()));
	}
	private void logError(Throwable t){
		log.error("Error ", t);
	}
}
